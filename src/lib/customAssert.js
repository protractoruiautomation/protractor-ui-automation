var customAssert=function(){
	var customLogger=require('./customLogger.js');
	var log=customLogger.logger("CustomAssert");
	
	/**
	 * This method use to assert true.
	 * 
	 * @author snarottam
	 * @param value
	 * @param msg
	 */ 
	this.assertTrue=function(value,msg){
		value.then(function(value){
			if(!value){
				log.error(msg)
			}
		});
		expect(value).toBe(true);
	}
	
	/**
	 * This method use to assert false.
	 * 
	 * @author snarottam
	 * @param value
	 * @param msg
	 */ 
	this.assertFalse=function(value,msg){
		value.then(function(value){
			if(value){
				log.error(msg)
			}
		});
		expect(value).toBe(false);
	}
	
	/**
	 * This method use to assert null.
	 * 
	 * @author snarottam
	 * @param value
	 * @param msg
	 */ 
	this.assertNull=function(value,msg){
		value.then(function(value){
			if(value!=null){
				log.error(msg)
			}
		});
		expect(value).toBeNull();
	}
	
	/**
	 * This method use to assert not null.
	 * 
	 * @author snarottam
	 * @param value
	 * @param msg
	 */ 
	this.assertNotNull=function(value,msg){
		value.then(function(value){
			if(value==null){
				log.error(msg)
			}
		});
		expect(value).not.toBeNull();
	}
	
	/**
	 * This method use to assert that actual is Equal to expected.
	 * 
	 * @author snarottam
	 * @param actual
	 * @param expected 
	 * @param msg
	 */ 
	this.assertEqual=function(actual,expected,msg){
			expected.then(function(expectedValue){
				if(expectedValue !=actual){
					log.error(msg)
				}	
			});
		expect(actual).toEqual(expected);
	}
	
	/**
	 * This method use to assert that actual is not Equal to expected.
	 * 
	 * @author snarottam
	 * @param actual
	 * @param expected
	 * @param msg
	 */ 
	this.assertNotEqual=function(actual,expected,msg){
			expected.then(function(expectedValue){
				if(actual==expectedValue){
					log.error(msg)
				}	
		});
		expect(actual).not.toEqual(expected);
	}
	
	/**
	 * This method use to assert that actual contains expected.
	 * 
	 * @author snarottam
	 * @param actual
	 * @param expected
	 * @param msg
	 */ 
	this.assertContain=function(actual,expected,msg){
			expecped.then(function(expectedValue){
				if(actual.indexOf(expectedValue)==-1){
					log.error(msg);
				}
			});
		expect(actual).toContain(expected);
	}
	
	/**
	 * This method use to assert that actual not contains expected.
	 * 
	 * @author snarottam
	 * @param actual
	 * @param expected
	 * @param msg
	 */ 
	this.assertNotContain=function(actual,expected,msg){
			expecped.then(function(expectedValue){
				if(actual.indexOf(expectedValue)!=-1){
					log.error(msg);
				}
			});
		expect(actual).not.toContain(expected);
	}
	
	/**
	 * This method use to verify true.
	 * 
	 * @author snarottam
	 * @param value
	 * @param msg
	 */ 
	this.verifyTrue=function(value,msg){
		value.then(function(value){
			if(!value){
				log.warn(msg)
			}
		});
	}
	
	/**
	 * This method use to verify fasle.
	 * 
	 * @author snarottam
	 * @param value
	 * @param msg
	 */ 
	this.verifyFalse=function(value,msg){
		value.then(function(value){
			if(value){
				log.warn(msg)
			}
		});
	}
	
	/**
	 * This method use to verify null.
	 * 
	 * @author snarottam
	 * @param value
	 * @param msg
	 */ 
	this.verifyNull=function(value,msg){
		value.then(function(value){
			if(value!=null){
				log.warn(msg)
			}
		});
	}
	
	/**
	 * This method use to verify not null.
	 * 
	 * @author snarottam
	 * @param value
	 * @param msg
	 */ 
	this.verifyNotNull=function(value,msg){
		value.then(function(value){
			if(value==null){
				log.warn(msg)
			}
		});
	}
	
	/**
	 * This method use to verify that actual is Equal to expected.
	 * 
	 * @author snarottam
	 * @param actual
	 * @param expected 
	 * @param msg
	 */ 
	this.verifyEqual=function(actual,expected,msg){
			expected.then(function(expectedValue){
				if(actual!=expectedValue){
					log.warn(msg)
				}	
			});
	}
	
	/**
	 * This method use to verify that actual is not Equal to expected.
	 * 
	 * @author snarottam
	 * @param actual
	 * @param expected 
	 * @param msg
	 */ 
	this.verifyNotEqual=function(actual,expected,msg){
			expected.then(function(expectedValue){
				if(actual==expectedValue){
					log.warn(msg)
				}	
			});
	}
	
	/**
	 * This method use to verify that actual contains expected.
	 * 
	 * @author snarottam
	 * @param actual
	 * @param expected
	 * @param msg
	 */ 
	this.verifyContain=function(actual,expected,msg){
			expecped.then(function(expectedValue){
				if(actual.indexOf(expectedValue)==-1){
					log.warn(msg);
				}
			});
	}
	
	/**
	 * This method use to verify that actual not contains expected.
	 * 
	 * @author snarottam
	 * @param actual
	 * @param expected
	 * @param msg
	 */ 
	this.verifyNotContain=function(actual,expected,msg){
			expecped.then(function(expectedValue){
				if(actual.indexOf(expectedValue)!=-1){
					log.warn(msg);
				}
			});
	}
	
}
module.exports = new customAssert();